FROM openjdk:8-jdk-alpine
COPY devschool-front-app-server-1.0.0.jar devschool-front-app-server-1.0.0.jar
EXPOSE 80/tcp
CMD ["java","-jar","devschool-front-app-server-1.0.0.jar"]
